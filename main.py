import datetime
import random
import sys


def split_first_tag(string, tag_name_start, tag_name_end):
    split = string.split(tag_name_start, 1)
    start = split[0]
    split2 = split[1].split(tag_name_end, 1)
    end = split2[1]
    content = split2[0]
    return start, content, end


def split_all_tags(string, tag_name_start, tag_name_end):
    split = string.split(tag_name_start)
    tag_list = []
    for i in range(1, len(split)):
        tag_list.append(split[i].split(tag_name_end, 1)[0])

    return tag_list


def split_html_file(name):
    html_file = open(name, "r")
    file_string = html_file.read()

    file_start, t_body, file_end = split_first_tag(file_string, "<tbody>", "</tbody>")

    split = split_all_tags(t_body, "<tr class=\"\">", "</tr>")
    machine_name = split_all_tags(split[0], "<td class=\"text-start\">", "</td>")[0]

    return [file_start, machine_name, file_end]


def join_html_file(split):
    tr_list = []

    for td_list in split[1]:
        td_list[1] = f"<span><span>{td_list[1]}</span></span>"

        for i in range(0, 5):
            td_list[i] = f"<td class=\"text-start\">{td_list[i]}</td>"

        td_list.insert(0, "<tr>")
        td_list.append("</tr>")

        tr_list.append("".join(td_list))

    return "".join([split[0], f"<tbody>{''.join(tr_list)}</tbody>", split[2]])


def convert_time(date, hours_minutes, seconds):
    return format_time(datetime.datetime.strptime(
        f"{date} {hours_minutes}:{seconds}", "%d.%m.%Y %H:%M:%S"))


def format_time(time):
    return time.strftime("%b %d, %Y %#I:%M:%S %p")


def main():
    split_html = split_html_file(sys.argv[1])
    html_end = split_html[2]

    for file_name in range(1, 26):
        html_start = split_html[0]
        t_body = []

        input_lines = open(f"input/{file_name}.csv").readlines()

        date = input_lines[1].split(";")[6]

        time_start = None
        time_end = None

        i = 0
        while input_lines[i + 10][0] != ";":
            row = input_lines[i + 10].split(";")
            t_body.append([split_html[1]])

            if time_start is None:
                time_start = convert_time(date, row[1], 0)

            time = convert_time(date, row[1], random.randint(0, 59))
            time_end = [date, row[1]]

            current = row[4].replace(",", ".")
            voltage = row[5].replace(",", ".")
            arc_time = row[6].replace(" ", "")

            voltage = "{:.2f}V".format(float(voltage[:-2]))
            current = "{:.2f}A".format(float(current[:-2]))

            t_body[i].extend([time, arc_time, current, voltage])

            i += 1

        time_end = convert_time(time_end[0], time_end[1], 59)
        creation_time = datetime.datetime.now() + datetime.timedelta(
            minutes=file_name * 5 + random.randint(4, 6), seconds=random.randint(0, 59))
        html_start = html_start.replace(
            "May 29, 2023 12:35:00 PM - May 29, 2023 1:17:59 PM",
            f"{time_start} - {time_end}").replace("Jun 14, 2023 10:47:22 AM", format_time(creation_time))

        file_string = join_html_file([html_start, t_body, html_end])
        file = open(f"{file_name}.html", "w")
        file.write(file_string)


if __name__ == '__main__':
    main()
